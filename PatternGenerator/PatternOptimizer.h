#pragma once

#include <string>
#include <memory>
#include <vector>
#include <random>
#include <cmath>
#include <ostream>
#include <functional>
#include <map>
#include <boost\filesystem.hpp>
#include "Tree.h"
#include "TreeWriter.h"
#include "TreeParser.h"
#include "RsRunner.h"


class ListingVisitor : public Visitor {
public:
	ListingVisitor(std::function<bool(std::shared_ptr<NodeAttribute>)> attrFilter);;

	std::vector<std::shared_ptr<NodeAttribute>> findAllAttrs(const FNodePtr &fNode);

	void visit(FNode & fNode) override;

	void visit(ENode & eNode) override;

	void visit(SNode & sNode) override;

	void visit(LNode & lNode) override;

private:
	std::vector<std::shared_ptr<NodeAttribute>> allAttrs;
	std::function<bool(std::shared_ptr<NodeAttribute>)> filter;

	void addAttrs(TreeNode &node);
};

class PKnotEntity {
public:
	std::shared_ptr<PseudoknotAttr> stemAttr_1;
	std::shared_ptr<PseudoknotAttr> stemAttr_2;

	std::string cons_1;
	std::string cons_2;
};

class PKnotAttrFinder : public Visitor {
public:

	std::vector<PKnotEntity> findPKnots(const FNodePtr &fNode);

	void visit(ENode & eNode) override;

	void visit(LNode & lNode) override;

private:
	std::vector<PKnotEntity> conses;

	void addAttrs(LeafNode &node);

	void addEntity(const std::shared_ptr<PseudoknotAttr> &knot, const LeafNode &node);
};

class PatternOptimizer {
public:
	static const int maxNonChangeCount = 1000;

	PatternOptimizer(std::ostream &log) : logStream(log) {}

	void optimize(const std::string &rsPath, const std::string &beginPatFile, const std::string &seqFile, const std::string &rsParams);

private:
	const std::string tempPatFile = "temp_pat.pat";

	std::ostream &logStream;

	std::vector<NodeAttribute> attrs;
	TreeWriter treeWriter;
	TreeParser treeParser;
	RsRunner rsRunner;

	double temperature = 1;
	double tempKoeff = 100;
	double currentOptValue = 1000000000000; // сделать нормальный double max
	std::shared_ptr<Tree> currentTree;
	std::shared_ptr<Tree> nextPossibleTree;

	void increaseTemperature();

	void initializeAnnealing(std::string beginPatFile);

	void modifyPattern(std::shared_ptr<Tree> &tree);

	double optFunction(const std::vector<RsResult> &rsResult);

	bool checkPseudoknots(const std::shared_ptr<Tree> &tree);
};

