#include "stdafx.h"
#include "Tree.h"

const std::string TreeNode::attrDelimiter = "    ";
const std::regex LengthAttr::lenRegexp = std::regex("len:(\\d+)\\.\\.(\\d+)");
const std::regex PseudoknotAttr::knotRegexp = std::regex(";pknot:(>|<)(\\w+)");

std::shared_ptr<Tree> copyTree(std::shared_ptr<Tree> &from) {
	TreeCopier copier;
	from->root()->acceptVisitor(copier);
	return copier.newTree;
}

void LengthAttr::readAttribute(const std::string & str) {
	std::smatch matchResult;
	if (std::regex_search(str, matchResult, lenRegexp)) {
		activeAttribute = true;
		min = std::stoi(matchResult[1]);
		max = std::stoi(matchResult[2]);
	}
}

void LengthAttr::modifyAttribute() {
	static std::uniform_int_distribution<int> random(0, 1);
	static std::default_random_engine randEngine;
	bool modifyMin = random(randEngine);
	if (modifyMin) {
		std::uniform_int_distribution<int> minDist(1, max);
		min = minDist(randEngine);
	}
	else {
		std::uniform_int_distribution<int> maxDist(min, 30);
		max = maxDist(randEngine);
	}
}

NodeAttrPtr LengthAttr::copy() const {
	std::shared_ptr<LengthAttr> newLen = std::make_shared<LengthAttr>();
	newLen->min = min;
	newLen->max = max;
	newLen->activeAttribute = activeAttribute;
	return newLen;
}

void PseudoknotAttr::readAttribute(const std::string & str) {
	std::smatch result;
	if (std::regex_search(str, result, knotRegexp)) {
		if (result[1] == '>') {
			dir = right;
		}
		else {
			dir = left;
		}
		id = result[2];
	}
}

std::string PseudoknotAttr::printAttribute() const {
	return std::string(";pknot:") + (dir == right ? ">" : "<") + id;
}

std::string PseudoknotAttr::getId() {
	return id;
}

direction PseudoknotAttr::getDirection() {
	return dir;
}

NodeAttrPtr PseudoknotAttr::copy() const {
	std::shared_ptr<PseudoknotAttr> newKnot = std::make_shared<PseudoknotAttr>();
	newKnot->id = id;
	newKnot->dir = dir;
	newKnot->activeAttribute = activeAttribute;
	return newKnot;
}

TreeNode::TreeNode(const CompositeNodePtr & parentNode) : parent(parentNode) {
	attrs.push_back(std::make_shared<LengthAttr>());
}

void TreeNode::readAttributes(const std::string & str) {
	for (NodeAttrPtr attr : attrs) {
		attr->readAttribute(str);
	}
}

CompositeNodePtr & TreeNode::getParent() {
	return parent;
}

TreeNode::attrs_iterator TreeNode::attrsBegin() {
	return attrs.begin();
}

TreeNode::attrs_iterator TreeNode::attrsEnd() {
	return attrs.end();
}

LeafNode::LeafNode(const CompositeNodePtr & parentNode) : TreeNode(parentNode) {
	attrs.push_back(std::make_shared<PseudoknotAttr>());
}

void LeafNode::setCons(const std::string & c) {
	cons = c;
}

std::string LeafNode::getCons() const {
	return cons;
}

TreeNode & CompositeNode::getChild(std::vector<TreeNodePtr>::size_type index) {
	return *childs.at(index);
}

void CompositeNode::addChild(TreeNodePtr child) {
	childs.push_back(child);
}

CompositeNode::childs_iterator CompositeNode::childsBegin() {
	return childs.cbegin();
}

CompositeNode::childs_iterator CompositeNode::childsEnd() {
	return childs.cend();
}

FNode::FNode(const CompositeNodePtr & parentNode) : CompositeNode(parentNode) {
	attrs.clear();
}

std::string FNode::getNodeName() const {
	return "F";
}

void FNode::acceptVisitor(Visitor & visitor) {
	visitor.visit(*this);
	for (auto node : childs) {
		node->acceptVisitor(visitor);
	}
	visitor.leave(*this);
}

std::string SNode::getNodeName() const {
	return "S";
}

void SNode::acceptVisitor(Visitor & visitor) {
	visitor.visit(*this);
	for (auto node : childs) {
		node->acceptVisitor(visitor);
	}
	visitor.leave(*this);
}

void SNode::setConsLeft(const std::string & c) {
	consLeft = c;
}

void SNode::setConsRight(const std::string & c) {
	consRight = c;
}

std::string SNode::getConsLeft() {
	return consLeft;
}

std::string SNode::getConsRight() {
	return consRight;
}

std::string LNode::getNodeName() const {
	return "L";
}

void LNode::acceptVisitor(Visitor & visitor) {
	visitor.visit(*this);
	visitor.leave(*this);
}

std::string ENode::getNodeName() const {
	return "E";
}

void ENode::acceptVisitor(Visitor & visitor) {
	visitor.visit(*this);
	visitor.leave(*this);
}

void TreeDestructor::visit(ENode & eNode) {
	eNode.getParent().reset();
}

void TreeDestructor::visit(SNode & sNode) {
	sNode.getParent().reset();
}

void TreeDestructor::visit(LNode & lNode) {
	lNode.getParent().reset();
}

Tree::Tree() : fNode(std::make_shared<FNode>(CompositeNodePtr(nullptr))) {}

FNodePtr Tree::root() const {
	return fNode;
}

Tree::~Tree() {
	TreeDestructor dest;
	fNode->acceptVisitor(dest);
}

void TreeCopier::visit(FNode & fNode) {
	copyAttrs(fNode, *(newTree->root()));
	currentNode = newTree->root();
}

void TreeCopier::visit(ENode & eNode) {
	ENodePtr newENode = std::make_shared<ENode>(currentNode);
	copyAttrs(eNode, *newENode);
	currentNode->addChild(newENode);
}

void TreeCopier::visit(SNode & sNode) {
	SNodePtr newSNode = std::make_shared<SNode>(currentNode);
	copyAttrs(sNode, *newSNode);
	currentNode->addChild(newSNode);
	currentNode = newSNode;
}

void TreeCopier::visit(LNode & lNode) {
	LNodePtr newLNode = std::make_shared<LNode>(currentNode);
	copyAttrs(lNode, *newLNode);
	currentNode->addChild(newLNode);
}

void TreeCopier::leave(SNode & sNode) {
	currentNode = currentNode->parent;
}

void TreeCopier::copyAttrs(TreeNode & from, TreeNode & to) {
	to.attrs.clear();
	std::for_each(from.attrsBegin(), from.attrsEnd(),
		[&to](NodeAttrPtr attr) {
		to.attrs.push_back(attr->copy());
	});
}


std::ostream & operator<<(std::ostream & os, const TreeNode & node) {
	os << node.getNodeName();
	for (const NodeAttrPtr attr : node.attrs) {
		os << TreeNode::attrDelimiter << attr->printAttribute();
	}
	return os;
}

bool NodeAttribute::isActiveAttribute() const {
	return activeAttribute;
}
