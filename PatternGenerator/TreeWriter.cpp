#include "stdafx.h"
#include "TreeWriter.h"

void WriterVisitor::visit(FNode & fNode) {
	printNode(fNode, deep);
	deep = 1;
}

void WriterVisitor::visit(ENode & eNode) {
	printNode(eNode, deep);
}

void WriterVisitor::visit(SNode & sNode) {
	printNode(sNode, deep);
	++deep;
}

void WriterVisitor::visit(LNode & lNode) {
	printNode(lNode, deep);
}

void WriterVisitor::leave(SNode & sNode) {
	--deep;
}

void WriterVisitor::printNode(TreeNode & node, int deep) {
	os << std::string(deep, ' ') << node << std::endl;
}

void TreeWriter::writeTree(std::shared_ptr<Tree> tree, std::ostream & os) {
	WriterVisitor visitor(os);
	os << "RNA_TREE_BEGIN" << std::endl;
	tree->root()->acceptVisitor(visitor);
	os << "RNA_TREE_END" << std::endl <<
		"PSEUDOKNOTS_BEGIN" << std::endl <<
		"PSEUDOKNOTS_END" << std::endl;
}
