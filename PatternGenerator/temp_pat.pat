RNA_TREE_BEGIN
F
 E    len:5..5    ;pknot:>
 S    len:3..6
  L    len:2..5    ;pknot:>
  S    len:3..6
   L    len:1..1    ;pknot:>
   S    len:3..6
    L    len:0..1    ;pknot:>
    S    len:3..6
     L    len:6..10    ;pknot:>
    L    len:0..1    ;pknot:>
    S    len:5..6
     L    len:6..10    ;pknot:>
    L    len:0..3    ;pknot:>
    S    len:3..6
     L    len:0..5    ;pknot:>
     S    len:3..6
      L    len:0..1    ;pknot:>
      S    len:3..17
       L    len:1..3    ;pknot:>
       S    len:3..6
        L    len:3..6    ;pknot:>k1
       L    len:1..3    ;pknot:>
      L    len:3..25    ;pknot:>
     L    len:8..12    ;pknot:>
     S    len:4..8
      L    len:3..6    ;pknot:>
      S    len:3..6
       L    len:3..6    ;pknot:>
      L    len:6..6    ;pknot:>
     L    len:1..1    ;pknot:<k2
    L    len:1..6    ;pknot:>
    S    len:3..6
     L    len:1..4    ;pknot:>
     S    len:3..6
      L    len:0..2    ;pknot:>
      S    len:1..3
       L    len:1..6    ;pknot:>k1
      L    len:3..6    ;pknot:>
      S    len:3..6
       L    len:3..6    ;pknot:>
       S    len:3..6
        L    len:8..12    ;pknot:<k2
        S    len:5..12
         L    len:3..6    ;pknot:>
         S    len:3..6
          L    len:7..12    ;pknot:>
         L    len:3..6    ;pknot:>
        L    len:1..4    ;pknot:>
        S    len:3..30
         L    len:0..2    ;pknot:>
         S    len:2..6
          L    len:0..1    ;pknot:>
          S    len:3..6
           L    len:0..2    ;pknot:>
           S    len:1..3
            L    len:7..11    ;pknot:>
           L    len:0..2    ;pknot:>
          L    len:10..15    ;pknot:>
         L    len:1..2    ;pknot:>
        L    len:0..8    ;pknot:>
       L    len:6..10    ;pknot:>
      L    len:0..1    ;pknot:>
     L    len:3..6    ;pknot:>
    L    len:6..10    ;pknot:>
   L    len:1..1    ;pknot:>
  L    len:1..3    ;pknot:>
 E    len:4..28    ;pknot:>
RNA_TREE_END
PSEUDOKNOTS_BEGIN
PSEUDOKNOTS_END
