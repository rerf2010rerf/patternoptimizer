#include "stdafx.h"
#include "TreeParser.h"


const std::regex TreeParser::eRegexp = std::regex("\\s+E.*");
const std::regex TreeParser::sRegexp = std::regex("\\s+S.*");
const std::regex TreeParser::lRegexp = std::regex("\\s+L.*");

std::shared_ptr<Tree> TreeParser::parse(std::istream & is) {
	string line;
	while (line.find("RNA_TREE_BEGIN") != 0) {
		getline(is, line);
	}
	return startParse(is);
}

std::shared_ptr<Tree> TreeParser::startParse(std::istream & is) {
	std::shared_ptr<Tree> result = std::make_shared<Tree>();

	string line;
	getline(is, line);
	std::shared_ptr<FNode> fNode = result->root();

	getline(is, line);
	std::shared_ptr<ENode> eNode = std::make_shared<ENode>(fNode); //��� ����� ��������, ��� �������� ������������� E ����
	eNode->readAttributes(line);
	fNode->addChild(eNode);

	parseTree(is, fNode);

	return result;
}

void TreeParser::parseTree(std::istream & is, std::shared_ptr<FNode> fNode) {
	string line;
	getline(is, line);
	int currentDeep = 1;
	std::shared_ptr<CompositeNode> currentNode = fNode;
	while (line.find("RNA_TREE_END") != 0) {
		int lineDeep = calculateDeep(line);
		if (lineDeep == currentDeep) {
			if (std::regex_match(line, sRegexp)) {
				addNode<SNode>(currentNode, line);
			}
			else {
				throw std::invalid_argument("illegal tree syntax: unexpected verticle type in line: " + line);
			}
		}
		else if (lineDeep == currentDeep + 1) {
			if (std::regex_match(line, lRegexp)) {
				++currentDeep;
				currentNode = std::static_pointer_cast<CompositeNode>(*--currentNode->childsEnd());
				addNode<LNode>(currentNode, line);
			}
			else {
				throw std::invalid_argument("illegal tree syntax: unexpected verticle type in line: " + line);
			}
		}
		else if (lineDeep = currentDeep - 1) {
			--currentDeep;
			//������������� ������ ����� ��������������� � ������������� ��������� 
			//���������� ��� �������� composite � leaf ������� � ���������
			currentNode = std::static_pointer_cast<CompositeNode>(currentNode->getParent());
			if (std::regex_match(line, lRegexp)) {
				addNode<LNode>(currentNode, line);
			}
			else if (std::regex_match(line, eRegexp)) {
				addNode<ENode>(currentNode, line);
				return;
			}
			else {
				throw std::invalid_argument("illegal tree syntax: unexpected verticle type in line: " + line);
			}
		}
		else {
			throw std::invalid_argument("illegal tree syntax: unexpected deep step in line: " + line);
		}

		getline(is, line);
	}
}

int TreeParser::calculateDeep(const std::string & str) {
	std::string::const_iterator result = std::find_if(str.cbegin(), str.cend(), [](const char c) {return c != ' '; });
	if (str.cend() != result) {
		return result - str.cbegin();
	}
	else {
		throw std::invalid_argument("illegal tree syntax: incorrect verticle deep in line: " + str);
	}
}
