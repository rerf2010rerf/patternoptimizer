#include "stdafx.h"
#include "RsRunner.h"

inline BracketParser::BracketParser(const std::vector<std::string>& seqs) : subSeqs(seqs) {}

void BracketParser::visit(ENode & eNode) {
	eNode.setCons(subSeqs[count]);
	++count;
}

void BracketParser::visit(SNode & sNode) {
	sNode.setConsLeft(subSeqs[count]);
	++count;
}

void BracketParser::visit(LNode & lNode) {
	lNode.setCons(subSeqs[count]);
	++count;
}

void BracketParser::leave(SNode & sNode) {
	sNode.setConsRight(subSeqs[count]);
	++count;
}

RsResult::RsResult(const std::vector<std::string>& res, const std::string & patFile) {
	std::string head = res.at(0);

	std::smatch matchRes;
	if (std::regex_search(head, matchRes, nameRegex)) {
		seqName = matchRes[1];
	}

	ES = matchInt(head, ESRegex);
	X = matchInt(head, XRegex);
	L = matchInt(head, LRegex);

	std::ifstream patStream(patFile);   //todo: � ����� ����� �� ������������ ���� � ���������, � ��� �� �� ������� ������ ��������?
	tree = treeParser.parse(patStream);
	patStream.close();  //todo: ������� �������� ��� ������ �������� � ������� shared_ptr ��� unique_ptr
	parseBrackets(res.at(2), res.at(3), res.at(1));
}

void RsResult::parseBrackets(const std::string & brackets, const std::string & pres, const std::string & seq) {
	std::vector<std::string> subSeqs;
	char last = pres.at(0);
	std::string::size_type lastEnd = 0;
	for (std::string::size_type i = 1; i < pres.length(); ++i) {
		if (last != pres.at(i)) {
			pushStem(subSeqs, seq.substr(lastEnd, i - lastEnd), brackets.substr(lastEnd, i - lastEnd));
			if (last != '.' && pres[i] != '.') {
				subSeqs.push_back("");
			}
			lastEnd = i;
		}
		last = pres.at(i);
	}
	pushStem(subSeqs, seq.substr(lastEnd), brackets.substr(lastEnd));

	BracketParser pars(subSeqs);
	tree->root()->acceptVisitor(pars);
}

void RsResult::pushStem(std::vector<std::string>& subSeqs, const std::string & seq, const std::string & brackets) {
	if (brackets.front() == brackets.back()) {
		subSeqs.push_back(seq);
	}
	else {
		std::string s1, s2;
		for (std::string::size_type i = 0; i < brackets.length(); ++i) {
			if (brackets[i] == '(') {
				s1.push_back(seq.at(i));
			}
			else {
				s2.push_back(seq.at(i));
			}
		}
		subSeqs.push_back(s1);
		subSeqs.push_back("");
		subSeqs.push_back(s2);
	}
}

inline int RsResult::matchInt(std::string & title, std::regex & regex) {
	std::smatch matchRes;
	if (std::regex_search(title, matchRes, regex)) {
		return std::stoi(matchRes[1]);
	}
	else {
		throw RsException("unexpected rs output format");
	}
}

std::vector<RsResult> RsRunner::runRs(const std::string & rsPath, const std::string & patFile, const std::string & seqFile, const std::string & params) {
	std::vector<RsResult> result;
	boost::filesystem::remove("errors.txt");
	boost::filesystem::remove("result.txt");

	//todo: �������� � ������� ������ ����� ����� ����� ����� ������, �� ���� ��������� ���. ���������, �����
	//�������� ��������
	bp::system(rsPath, seqFile, patFile, params, bp::std_out > "result.txt", bp::std_err > "errors.txt");

	std::string line;
	std::vector<std::string> nextResult;

	std::ifstream resStream("result.txt"); //todo: �����������, ��� ��������� �������� � ������� ����� ����������
	std::ifstream errStream("errors.txt");
	if (std::getline(errStream, line)) {
		throw RsException(line.c_str());
	}
	while (std::getline(resStream, line)) {
		if (line.length() > 1) {
			nextResult.push_back(line);
		}
		else {
			result.push_back(RsResult(nextResult, patFile));
			nextResult.clear();
		}
	}

	resStream.close();
	errStream.close();

	return result;
}

void RsRunner::close_ifstream(std::ifstream * is) {
	is->close();
}
