#pragma once

#include <istream>
#include <memory>
#include <string>
#include <regex>
#include <algorithm>
#include <exception>
#include <type_traits>
#include "Tree.h"

class TreeParser
{
public:
	using string = std::string;

	std::shared_ptr<Tree> parse(std::istream & is);

private:
	const static std::regex eRegexp;
	const static std::regex sRegexp;
	const static std::regex lRegexp;

	std::shared_ptr<Tree> startParse(std::istream & is);

	void parseTree(std::istream & is, std::shared_ptr<FNode> fNode);


	template<class Node> 
	std::enable_if_t<std::is_base_of<TreeNode, Node>::value, void>
	addNode(std::shared_ptr<CompositeNode> & currentNode, const std::string & line) {
		std::shared_ptr<Node> node = std::make_shared<Node>(currentNode);
		node->readAttributes(line);
		currentNode->addChild(node);
	}

	int calculateDeep(const std::string & str);
};

