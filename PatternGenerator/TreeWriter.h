#pragma once

#include <ostream>
#include <memory>
#include <vector>
#include <stack>
#include <algorithm>
#include <map>
#include <string>
#include "Tree.h"

class WriterVisitor : public Visitor {
public:
	WriterVisitor(std::ostream & stream) : os(stream) {}

	void visit(FNode & fNode) override;
	
	void visit(ENode & eNode) override;

	void visit(SNode & sNode) override;

	void visit(LNode & lNode) override;

	void leave(SNode & sNode) override;

private:
	std::ostream & os;
	int deep = 0;

	void printNode(TreeNode & node, int deep);
};

class TreeWriter {
public:
	void writeTree(std::shared_ptr<Tree> tree, std::ostream & os);
};