#pragma once

#include <string>
#include <vector>
#include <exception>
#include <regex>
#include <memory>
#include <iostream>
#include <boost\process.hpp>
#include "Tree.h"
#include "TreeParser.h"

namespace bp = boost::process;

class RsException : public std::exception {
public:
	RsException(char const* const _Message) : exception(_Message) {};
};

class BracketParser : public Visitor {
public:
	BracketParser(const std::vector<std::string> &seqs);

	void visit(ENode & eNode) override;
	
	void visit(SNode & sNode) override;

	void visit(LNode & lNode) override;

	void leave(SNode &sNode) override;

private:
	const std::vector<std::string> subSeqs;
	int count = 0;
};

class RsResult {
public: 
	std::string seqName;
	std::shared_ptr<Tree> tree;
	int ES;
	int X;
	int L;

	RsResult(const std::vector<std::string> &res, const std::string &patFile);

private:
	TreeParser treeParser;

	std::regex nameRegex = std::regex(">NM\\:\\[(.*)\\]");
	std::regex ESRegex = std::regex("ES\\:(.+)\\s");
	std::regex XRegex = std::regex("X\\:(\\d+)\\s");
	std::regex LRegex = std::regex("L\\:(\\d+)\\s");

	void parseBrackets(const std::string &brackets, const std::string &pres, const std::string &seq);

	void pushStem(std::vector<std::string> &subSeqs, const std::string &seq, const std::string &brackets);

	int matchInt(std::string &title, std::regex &regex);
};

class RsRunner {
public:
	std::vector<RsResult> runRs(const std::string &rsPath, 
		const std::string &patFile, 
		const std::string &seqFile, 
		const std::string &params);

private:
	void close_ifstream(std::ifstream *is);
};