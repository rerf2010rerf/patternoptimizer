// PatternGenerator.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <regex>
#include "Tree.h"
#include "TreeParser.h"
#include "TreeWriter.h"
#include "RsRunner.h"
#include "PatternOptimizer.h"


int main()
{

	std::ofstream log("log.txt");
	PatternOptimizer optimizer(log);
	optimizer.optimize("e:\\учеба\\rs.exe", "e:\\учеба\\p.lit_LSU1.pat", "e:\\учеба\\pyl_lsu_1.fa",
		"-o:e:\\учеба\\rs.cfg");

    return 0;
}

