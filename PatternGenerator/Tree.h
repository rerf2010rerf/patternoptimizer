#pragma once

#include <vector>
#include <memory>
#include <ostream>
#include <string>
#include <regex>
#include <algorithm>
#include <random>

class TreeNode;
class CompositeNode;
class LeafNode;
class FNode;
class ENode;
class SNode;
class LNode;

using TreeNodePtr = std::shared_ptr<TreeNode>;
using CompositeNodePtr = std::shared_ptr<CompositeNode>;
using LeafNodePtr = std::shared_ptr<LeafNode>;
using FNodePtr = std::shared_ptr<FNode>;
using ENodePtr = std::shared_ptr<ENode>;
using SNodePtr = std::shared_ptr<SNode>;
using LNodePtr = std::shared_ptr<LNode>;

class NodeAttribute;
using NodeAttrPtr = std::shared_ptr<NodeAttribute>;


class Visitor {
public:
	virtual void visit(FNode & fNode) {}
	virtual void visit(ENode & eNode) {}
	virtual void visit(SNode & sNode) {}
	virtual void visit(LNode & lNode) {}

	virtual void leave(FNode & fNode) {}
	virtual void leave(ENode & eNode) {}
	virtual void leave(SNode & sNode) {}
	virtual void leave(LNode & lNode) {}
};

class NodeAttribute {
public:	
	NodeAttribute() {};

	bool isActiveAttribute() const;

	//todo: ������ �������� ��������� � TreeParser
	virtual void readAttribute(const std::string & str) = 0;

	virtual std::string printAttribute() const = 0;

	virtual void modifyAttribute() {};

	virtual NodeAttrPtr copy() const = 0;

protected:
	bool activeAttribute = false;
};

class LengthAttr : public NodeAttribute {
public:
	using NodeAttribute::NodeAttribute;

	void readAttribute(const std::string & str) override;

	std::string printAttribute() const override {
		return "len:" + std::to_string(min) + ".." + std::to_string(max);
	}

	void modifyAttribute() override;

	NodeAttrPtr copy() const override;

private:
	int min;
	int max;

	const static std::regex lenRegexp;  // ������� ��������
};

enum direction {
	right,
	left
};

class PseudoknotAttr : public NodeAttribute {
public:
	using NodeAttribute::NodeAttribute;

	void readAttribute(const std::string &str) override;

	std::string printAttribute() const override;

	std::string getId();

	direction getDirection();

	NodeAttrPtr copy() const override;

private:
	std::string id;
	direction dir;

	const static std::regex knotRegexp;
};

class TreeCopier;

class TreeNode
{
	friend TreeCopier;
	friend std::ostream & operator<<(std::ostream & os, const TreeNode & node);
public:
	using attrs_iterator = std::vector<NodeAttrPtr>::iterator;

	TreeNode(const CompositeNodePtr & parentNode);

	void readAttributes(const std::string & str);

	CompositeNodePtr &getParent();

	attrs_iterator attrsBegin();

	attrs_iterator attrsEnd();

	virtual std::string getNodeName() const = 0;

	virtual void acceptVisitor(Visitor & visitor) = 0;

	virtual ~TreeNode() = default;

protected:
	std::vector<NodeAttrPtr> attrs;

private:
	const static std::string attrDelimiter;
	CompositeNodePtr parent;
};

class LeafNode : public TreeNode {
public:

	LeafNode(const CompositeNodePtr & parentNode);
	
	void setCons(const std::string &c);

	std::string getCons() const;
private:
	std::string cons;
};

class CompositeNode : public TreeNode {
public:
	using TreeNode::TreeNode;
	using childs_iterator = std::vector<TreeNodePtr>::const_iterator;

	TreeNode & getChild(std::vector<TreeNodePtr>::size_type index);

	void addChild(TreeNodePtr child);

	childs_iterator childsBegin();

	childs_iterator childsEnd();

protected:
	std::vector<TreeNodePtr> childs;
};

class FNode : public CompositeNode {
public:
	FNode(const CompositeNodePtr & parentNode);;

	std::string getNodeName() const override;

	void acceptVisitor(Visitor & visitor) override;
};

class SNode : public CompositeNode {
public:
	using CompositeNode::CompositeNode;

	std::string getNodeName() const override;

	void acceptVisitor(Visitor & visitor) override;

	void setConsLeft(const std::string &c);

	void setConsRight(const std::string &c);

	std::string getConsLeft();

	std::string getConsRight();

private:
	std::string consLeft;
	std::string consRight;
};

class LNode : public LeafNode {
public:
	using LeafNode::LeafNode;

	std::string getNodeName() const override;

	void acceptVisitor(Visitor & visitor) override;
};

class ENode : public LeafNode {
public:
	using LeafNode::LeafNode;

	std::string getNodeName() const override;

	void acceptVisitor(Visitor & visitor) override;
};


class TreeDestructor : public Visitor {
public:
	virtual void visit(ENode & eNode) override;
	virtual void visit(SNode & sNode) override;
	virtual void visit(LNode & lNode) override;
};

class Tree {
public:
	Tree();;

	FNodePtr root() const;

	~Tree();

private:
	FNodePtr fNode;
};

class TreeCopier : public Visitor {
public:
	std::shared_ptr<Tree> newTree = std::make_shared<Tree>();

	void visit(FNode & fNode) override;

	void visit(ENode & eNode) override;

	void visit(SNode & sNode) override;

	void visit(LNode & lNode) override;

	void leave(SNode & sNode) override;

private:
	CompositeNodePtr currentNode;

	void copyAttrs(TreeNode &from, TreeNode &to);
};

std::shared_ptr<Tree> copyTree(std::shared_ptr<Tree> &fNode);
