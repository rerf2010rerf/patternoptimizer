#include "stdafx.h"
#include "PatternOptimizer.h"

ListingVisitor::ListingVisitor(std::function<bool(std::shared_ptr<NodeAttribute>)> attrFilter) : filter(attrFilter) {}

std::vector<std::shared_ptr<NodeAttribute>> ListingVisitor::findAllAttrs(const FNodePtr & fNode) {
	allAttrs.clear();
	fNode->acceptVisitor(*this);
	return allAttrs;
}

void ListingVisitor::visit(FNode & fNode) {
	addAttrs(fNode);
}

void ListingVisitor::visit(ENode & eNode) {
	addAttrs(eNode);
}

void ListingVisitor::visit(SNode & sNode) {
	addAttrs(sNode);
}

void ListingVisitor::visit(LNode & lNode) {
	addAttrs(lNode);
}

void ListingVisitor::addAttrs(TreeNode & node) {
	std::for_each(node.attrsBegin(), node.attrsEnd(), [this](auto attr) {
		if (filter(attr)) {
			allAttrs.push_back(attr);
		}
	});
}

std::vector<PKnotEntity> PKnotAttrFinder::findPKnots(const FNodePtr & fNode) {
	conses.clear();
	fNode->acceptVisitor(*this);
	return conses;
}

void PKnotAttrFinder::visit(ENode & eNode) {
	addAttrs(eNode);
}

void PKnotAttrFinder::visit(LNode & lNode) {
	addAttrs(lNode);
}

void PKnotAttrFinder::addAttrs(LeafNode & node) {
	std::for_each(node.attrsBegin(), node.attrsEnd(), [this, &node](auto attr) {
		std::shared_ptr<PseudoknotAttr> knot = std::dynamic_pointer_cast<PseudoknotAttr>(attr);
		if (nullptr != knot && !knot->getId().empty()) {
			addEntity(knot, node);
		}
	});
}

void PKnotAttrFinder::addEntity(const std::shared_ptr<PseudoknotAttr>& knot, const LeafNode & node) {
	for (PKnotEntity &entity : conses) {
		if (entity.stemAttr_1->getId() == knot->getId()) {
			entity.stemAttr_2 = knot;
			entity.cons_2 = node.getCons();
			return;
		}
	}
	PKnotEntity entity;
	entity.stemAttr_1 = knot;
	entity.cons_1 = node.getCons();
	conses.push_back(entity);
}

void PatternOptimizer::optimize(const std::string & rsPath, const std::string & beginPatFile, const std::string & seqFile, const std::string & rsParams) {
	int nonChangeCount = 0;
	initializeAnnealing(beginPatFile);
	while (nonChangeCount < maxNonChangeCount) {
		++nonChangeCount;
		nextPossibleTree = copyTree(currentTree);
		modifyPattern(nextPossibleTree);
		std::ofstream tempPatStream(tempPatFile);  //���� ������� ���������� �������� ���� ��� ������
		treeWriter.writeTree(nextPossibleTree, tempPatStream);
		tempPatStream.close();
		logStream << "running rs... This current optValue: " << currentOptValue << std::endl
			<< "next solution" << std::endl;
		treeWriter.writeTree(nextPossibleTree, logStream);
		std::vector<RsResult> rsResult = rsRunner.runRs(rsPath,
			tempPatFile, seqFile, rsParams);
		double nextPossibleOptValue = optFunction(rsResult);
		logStream << "rs finished. nextPossibleOptValue: " << nextPossibleOptValue << std::endl;
		if (currentOptValue >= nextPossibleOptValue) {
			currentTree = nextPossibleTree;
			currentOptValue = nextPossibleOptValue;
			nonChangeCount = 0;
			logStream << "new solution accepted, currentOptValue: " << currentOptValue << std::endl;
		}
		else {
			static std::uniform_real_distribution<double> dist(0, 1);
			static std::default_random_engine engine;
			double annealFunc = std::exp((currentOptValue - nextPossibleOptValue) / (tempKoeff * temperature));
			double rand = dist(engine);
			if (annealFunc > rand) {
				currentTree = nextPossibleTree;
				currentOptValue = nextPossibleOptValue;
				nonChangeCount = 0;
				logStream << "new solution accepted, currentOptValue: " << currentOptValue << std::endl;
			}
		}
		increaseTemperature();
	}
}

void PatternOptimizer::increaseTemperature() {
	temperature *= 0.99;
}

void PatternOptimizer::initializeAnnealing(std::string beginPatFile) {
	boost::filesystem::remove(boost::filesystem::path(tempPatFile));
	boost::filesystem::copy(boost::filesystem::path(beginPatFile), boost::filesystem::path(tempPatFile));
	std::ifstream ifs(tempPatFile);
	currentTree = treeParser.parse(ifs);
	ifs.close();  //����� ���������, ���� ���� ��������� ������...
}

void PatternOptimizer::modifyPattern(std::shared_ptr<Tree>& tree) {
	ListingVisitor visitor([](std::shared_ptr<NodeAttribute> attr) { return attr->isActiveAttribute(); });
	std::vector<std::shared_ptr<NodeAttribute>> attrs = visitor.findAllAttrs(tree->root());
	static std::uniform_int_distribution<unsigned> randomDist(0, attrs.size() - 1);
	static std::default_random_engine randEngine;

	attrs.at(randomDist(randEngine))->modifyAttribute();
}

double PatternOptimizer::optFunction(const std::vector<RsResult>& rsResult) {
	const int minInt = -10000;
	int maxES = minInt;
	for (const RsResult res : rsResult) {
		checkPseudoknots(res.tree);
		if (res.X == 0 && res.ES > maxES && checkPseudoknots(res.tree)) {
			maxES = res.ES;
		}
	}

	int result = maxES != minInt ? -maxES : 100000; //��� ���� ����� ������� ���������� ���� ����.
	logStream << "evaluated optFunc: " << result << std::endl;

	return result;
}

bool PatternOptimizer::checkPseudoknots(const std::shared_ptr<Tree>& tree) {
	PKnotAttrFinder knotFinder;
	std::vector<PKnotEntity> knots = knotFinder.findPKnots(tree->root());

	return true;
}
